//khai báo thư viện express Js
const express = require("express");
//khai báo router chạy
const router = express.Router();
//khai báo dice history controller
const diceHisController = require("../controllers/diceHistoryController")

router.post("/devcamp-lucky-dice/dice-history" , diceHisController.createDiceHistory);
router.get("/dice-histories", diceHisController.getAllDiceHis);
router.get("/dice-histories/:diceHisId", diceHisController.getDiceHisById);
router.put("/dice-histories/:diceHisId", diceHisController.updateDiceHisById)
router.delete("/dice-histories/:diceHisId", diceHisController.deleteDiceHisById);

router.get("/devcamp-lucky-dice/dice-history", diceHisController.getDiceHistoryByUsername)

module.exports = router;
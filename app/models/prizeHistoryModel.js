//import thư viện mongoose
const mongoose = require("mongoose");
//import class schema từ thư viện mongoose
const Schema = mongoose.Schema;

//tạo class prize history Schema 
const prizeHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    username: {
        type: String, 
        ref: "Users"
    },
	prize: {
        type: String,
        ref: "Prize"
    },
	createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});
module.exports = mongoose.model("prizehistory", prizeHistorySchema);
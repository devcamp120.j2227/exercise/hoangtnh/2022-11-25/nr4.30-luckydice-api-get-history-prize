//import thư viện mongoose
const mongoose = require("mongoose");
//import thư viện schema class
const Schema = mongoose.Schema;

//khai báo user Schema
const userSchema = new Schema ({
    username:{
        type:String,
        unique: true,
        required: true
    },
	firstname:{
        type: String, 
        required: true
    },
	lastname: {
        type: String, 
        required: true
    },
	createdAt: {
        type:Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    },
    dicehistory:[{
        type: mongoose.Types.ObjectId,
        ref: "diceHistory"
    }],
    prizehistory: [{
        type: mongoose.Types.ObjectId,
        ref: "prizehistory"
    }]
});
module.exports = mongoose.model("Users",userSchema);